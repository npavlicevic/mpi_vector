#ifndef MPI_VECTOR_H
#define MPI_VECTOR_H
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#define MAX_LOCAL_ORDER 100
typedef float LOCAL_MATRIX_T[MAX_LOCAL_ORDER][MAX_LOCAL_ORDER];
float mpi_serial_dot(float local_x[], float local_y[], int n);
float mpi_parallel_dot(float local_x[], float local_y[], int n);
void mpi_parallel_dot_matrix(LOCAL_MATRIX_T local_m, float local_v[], float global_v[], float result_v[], int _local_m, int _local_n, int m, int n);
void mpi_parallel_matrix_product(LOCAL_MATRIX_T local_a, LOCAL_MATRIX_T local_b, LOCAL_MATRIX_T local_c, int m, int n, int r, int p);
void mpi_read_vector(float local_v[], int n, int p, int my_rank);
void mpi_read_vector_scatter(float local_v[], int n, int p, int my_rank);
void mpi_print_vector(float local_v[], int n, int p, int my_rank);
void mpi_print_vector_local(float local_v[], int n);
void mpi_random_vector(float local_v[], int n);
void mpi_scatter_vector(float _local_v[], float local_v[], int n);
void mpi_gather_vector(float all_v[], float local_v[], int n);
void mpi_broadcast_vector(float local_v[], int n);
void mpi_all_to_all_vector(float _local_v[], float local_v[], int n);
void mpi_read_matrix(float local_m[], int n, int p, int my_rank);
void mpi_read_matrix_scatter(LOCAL_MATRIX_T local_m, int m, int n, int p, int my_rank);
void mpi_print_matrix(LOCAL_MATRIX_T local_m, int m, int n, int p, int my_rank);
int mpi_float_compare(const void *a, const void *b);
#endif
