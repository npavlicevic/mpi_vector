#include "mpi_vector.h"
main(int argc, char **argv) {
  // this is run by every process
  // scatter gather
  // scatter input to all processes
  int p;
  int my_rank;
  int n;
  int m;
  int n_bar;
  int m_bar;
  float local_v[MAX_LOCAL_ORDER];
  LOCAL_MATRIX_T local_m;
  float global_v[MAX_LOCAL_ORDER];
  float result_v[MAX_LOCAL_ORDER];
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  fscanf(stdin, "%d", &m);
  MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
  fscanf(stdin, "%d", &n);
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
  m_bar = m / p;
  n_bar = n / p;
  mpi_read_vector_scatter(local_v, n_bar, p, my_rank);
  mpi_print_vector(local_v, n_bar, p, my_rank);
  mpi_read_matrix_scatter(local_m, m_bar, n, p, my_rank);
  mpi_print_matrix(local_m, m_bar, n, p, my_rank);
  mpi_parallel_dot_matrix(local_m, local_v, global_v, result_v, m_bar, n_bar, m, n);
  mpi_print_vector(result_v, m_bar, p, my_rank);
  MPI_Finalize();
}
