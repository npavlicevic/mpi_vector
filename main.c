#include "mpi_vector.h"
main(int argc, char **argv) {
  // this is run by every process
  // split input between processes
  float local_x[MAX_LOCAL_ORDER];
  float local_y[MAX_LOCAL_ORDER];
  int n;
  int n_bar;
  float dot;
  int p;
  int my_rank;
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  fscanf(stdin, "%d", &n);
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
  n_bar = n / p;
  mpi_read_vector(local_x, n_bar, p, my_rank);
  mpi_read_vector(local_y, n_bar, p, my_rank);
  dot = mpi_parallel_dot(local_x, local_y, n_bar);
  if(!my_rank) {
    printf("the dot product is %f\n", dot);
  }
  MPI_Finalize();
}
