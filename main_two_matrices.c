#include "mpi_vector.h"
main(int argc, char **argv) {
  int my_rank;
  int p;
  int m;
  int n;
  int r;
  int m_bar;
  int n_bar;
  LOCAL_MATRIX_T local_a;
  LOCAL_MATRIX_T local_b;
  LOCAL_MATRIX_T local_c;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  fscanf(stdin, "%d", &m);
  MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
  m_bar = m / p;
  fscanf(stdin, "%d", &n);
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
  n_bar = n / p;
  fscanf(stdin, "%d", &r);
  MPI_Bcast(&r, 1, MPI_INT, 0, MPI_COMM_WORLD);
  mpi_read_matrix_scatter(local_a, m_bar, n, p, my_rank);
  mpi_print_matrix(local_a, m_bar, n, p, my_rank);
  mpi_read_matrix_scatter(local_b, n_bar, r, p, my_rank);
  mpi_print_matrix(local_b, n_bar, r, p, my_rank);
  mpi_parallel_matrix_product(local_a, local_b, local_c, m_bar, n_bar, r, p);
  mpi_print_matrix(local_c, m_bar, r, p, my_rank);
  MPI_Finalize();
}
