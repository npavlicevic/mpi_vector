#
# Makefile 
# mpi vector dot product
#

CC=mpicc
CCLIB=gcc
CCAR=ar
LIBS=-lmpi
FLAGSLIB=-static -c
FLAGSAR=rcs
FILES=mpi_vector.c main.c
FILES_MATRIX=mpi_vector.c main_matrix.c
FILES_TWO_MATRICES=mpi_vector.c main_two_matrices.c
FILES_LIB=mpi_vector.c
FILES_LIB_O=mpi_vector.o
REMOVE=main main_matrix main_two_matrices

all: main matrix two_matrices lib libar

main: ${FILES}
	${CC} $^ -o main ${LIBS}

matrix: ${FILES_MATRIX}
	${CC} $^ -o main_matrix ${LIBS}

two_matrices: ${FILES_TWO_MATRICES}
	${CC} $^ -o main_two_matrices ${LIBS}

lib: ${FILES_LIB}
	${CC} ${FLAGSLIB} $^ -o mpi_vector.o ${LIBS}

libar: ${FILES_LIB_O}
	${CCAR} ${FLAGSAR} libmpivector.a $^

clean: ${REMOVE}
	rm $^
