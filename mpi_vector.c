#include "mpi_vector.h"
float mpi_serial_dot(float local_x[], float local_y[], int n) {
  float sum = 0.0;
  int i = 0;
  for(; i < n; i++) {
    sum += local_x[i] * local_y[i];
  }
  return sum;
}
float mpi_parallel_dot(float local_x[], float local_y[], int n) {
  float dot = 0.0;
  float local_dot;
  local_dot = mpi_serial_dot(local_x, local_y, n);
  MPI_Reduce(&local_dot, &dot, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);
  return dot;
}
void mpi_parallel_dot_matrix(LOCAL_MATRIX_T local_m, float local_v[], float global_v[], float result_v[], int _local_m, int _local_n, int m, int n) {
  int i, j;
  MPI_Allgather(local_v, _local_n, MPI_FLOAT, global_v, _local_n, MPI_FLOAT, MPI_COMM_WORLD);
  // gather vector to single array
  for(i = 0; i < _local_m; i++) {
    result_v[i] = 0.0;
    for(j = 0; j < n; j++) {
      result_v[i] += (local_m[i][j] * global_v[j]);
    }
  }
}
void mpi_parallel_matrix_product(LOCAL_MATRIX_T local_a, LOCAL_MATRIX_T local_b, LOCAL_MATRIX_T local_c, int m, int n, int r, int p) {
  LOCAL_MATRIX_T _local_b;
  MPI_Allgather(local_b, n * MAX_LOCAL_ORDER, MPI_FLOAT, _local_b, n * MAX_LOCAL_ORDER, MPI_FLOAT, MPI_COMM_WORLD);
  int i, j, k;
  float sum;
  for(i = 0; i < m; i++) {
    for(k = 0; k < r; k++) {
      sum = 0;
      for(j = 0; j < n * p; j++) {
        sum += local_a[i][j] * _local_b[j][k];
      }
      local_c[i][k] = sum;
    }
  }
}
void mpi_read_vector(float local_v[], int n, int p, int my_rank) {
  int i;
  int q;
  float _local_v[MAX_LOCAL_ORDER];
  MPI_Status status;
  if(!my_rank) {
    for(i = 0; i < n; i++) {
      fscanf(stdin, "%f", &local_v[i]);
    }
    for(q = 1; q < p; q++) {
      for(i = 0; i < n; i++) {
        fscanf(stdin, "%f", &_local_v[i]);
      }
      MPI_Send(_local_v, n, MPI_FLOAT, q, 0, MPI_COMM_WORLD);
    }
  } else {
    MPI_Recv(local_v, n, MPI_FLOAT, 0, 0, MPI_COMM_WORLD, &status);
  }
}
void mpi_read_vector_scatter(float local_v[], int n, int p, int my_rank) {
  // read everything
  // send chunks of array to other processes
  int i;
  float _local_v[MAX_LOCAL_ORDER];
  if(!my_rank) {
    for(i = 0; i < n * p; i++) {
      fscanf(stdin, "%f", &_local_v[i]);
    }
  }
  MPI_Scatter(_local_v, n, MPI_FLOAT, local_v, n, MPI_FLOAT, 0, MPI_COMM_WORLD);
}
void mpi_print_vector(float local_v[], int n, int p, int my_rank) {
  int i;
  float _local_v[MAX_LOCAL_ORDER];
  MPI_Gather(local_v, n, MPI_FLOAT, _local_v, n, MPI_FLOAT, 0, MPI_COMM_WORLD);
  if(!my_rank) {
    for(i = 0; i < n * p; i++) {
      printf("%f ", _local_v[i]);
    }
    printf("\n");
  }
}
void mpi_print_vector_local(float local_v[], int n) {
  int i;
  for(i = 0; i < n; i++) {
    printf("%f ", local_v[i]);
  }
  printf("\n");
}
void mpi_random_vector(float local_v[], int n) {
  int i;
  for(i = 0; i < n; i++) {
    local_v[i] = rand();
  }
}
void mpi_scatter_vector(float _local_v[], float local_v[], int n) {
  MPI_Scatter(local_v, n, MPI_FLOAT, _local_v, n, MPI_FLOAT, 0, MPI_COMM_WORLD);
}
void mpi_gather_vector(float all_v[], float local_v[], int n) {
  MPI_Gather(local_v, n, MPI_FLOAT, all_v, n, MPI_FLOAT, 0, MPI_COMM_WORLD);
}
void mpi_broadcast_vector(float local_v[], int n) {
  MPI_Bcast(local_v, n, MPI_FLOAT, 0, MPI_COMM_WORLD);
}
void mpi_all_to_all_vector(float _local_v[], float local_v[], int n) {
  MPI_Alltoall(local_v, n, MPI_FLOAT, _local_v, n, MPI_FLOAT, MPI_COMM_WORLD);
}
void mpi_read_matrix(float local_m[], int n, int p, int my_rank) {
  int i;
  int q;
  float _local_m[MAX_LOCAL_ORDER];
  MPI_Status status;
  if(!my_rank) {
    for(i = 0; i < n; i++) {
      fscanf(stdin, "%f", &local_m[i]);
    }
    for(q = 1; q < p; q++) {
      for(i = 0; i < n; i++) {
        fscanf(stdin, "%f", &_local_m[i]);
      }
      MPI_Send(_local_m, n, MPI_FLOAT, q, 0, MPI_COMM_WORLD);
    }
  } else {
    MPI_Recv(_local_m, n, MPI_FLOAT, 0, 0, MPI_COMM_WORLD, &status);
  }
}
void mpi_read_matrix_scatter(LOCAL_MATRIX_T local_m, int m, int n, int p, int my_rank) {
  // read whole array in
  // broadcast chunks
  int i, j;
  LOCAL_MATRIX_T _local_m;
  if(!my_rank) {
    for(i = 0; i < m * p; i++) {
      for(j = 0; j < n; j++) {
        fscanf(stdin, "%f", &_local_m[i][j]);
      }
    }
  }
  MPI_Scatter(_local_m, m * MAX_LOCAL_ORDER, MPI_FLOAT, local_m, m * MAX_LOCAL_ORDER, MPI_FLOAT, 0, MPI_COMM_WORLD);
}
void mpi_print_matrix(LOCAL_MATRIX_T local_m, int m, int n, int p, int my_rank) {
  int i, j;
  LOCAL_MATRIX_T _local_m;
  MPI_Gather(local_m, m * MAX_LOCAL_ORDER, MPI_FLOAT, _local_m, m * MAX_LOCAL_ORDER, MPI_FLOAT, 0, MPI_COMM_WORLD);
  if(!my_rank) {
    for(i = 0; i < m * p; i++) {
      for(j = 0; j < n; j++) {
        printf("%f ", _local_m[i][j]);
      }
      printf("\n");
    }
  }
}
int mpi_float_compare(const void *a, const void *b) {
  float *_a = (float*)a;
  float *_b = (float*)b;
  if(*_a < *_b) {
    return -1;
  }
  if(*_a > *_b) {
    return 1;
  }
  return 0;
}
